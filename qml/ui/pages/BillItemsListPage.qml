/*
 * Copyright (C) 2019  Johannes Renkl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * ubuntu-calculator-app is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.7
import Ubuntu.Components 1.3 as UITK
import Ubuntu.Components.Popups 1.3 as UITK_Popups
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import Ubility 1.0
import "../components"

UITK.Page {
    id: itemsListPage

    property var billItems: null
    property string clientName: ""
    property var billId: null
    property var products: null

    onBillIdChanged: {
        clientName = "";
    }

    signal reprintBill()
    signal printBill()
    signal addBillItem()

    header: UITK.PageHeader {
        id: header
        title: i18n.tr("Bill Items")
        subtitle: clientName != "" ? clientName : i18n.tr("Bill %1").arg(billId)

        trailingActionBar.actions: [
//            UITK.Action {
//                id: addBillItem
//                objectName: "addBillItem"
//                text: i18n.tr("Add Item")
//                iconName: "add"
//                onTriggered: {
//                    itemsListPage.addBillItem()
//                }
//            },
            UITK.Action {
                id: printBill
                objectName: "printBill"
                text: i18n.tr("Print Bill")
                iconName: "document-print"
                onTriggered: {
                    clientName != "" ? itemsListPage.printBill() : itemsListPage.reprintBill()
                }
            }
        ]
    }

    ColumnLayout {
        anchors.topMargin: header.height
        anchors.bottomMargin: Qt.inputMethod.visible ? Qt.inputMethod.keyboardRectangle.height : 0

        anchors.fill: parent
        spacing: units.gu(3)

        ScrollView {
            Layout.fillHeight: true
            Layout.fillWidth: true
            Layout.alignment: Qt.AlignTop

            ListView {
                id: itemsListView
                anchors.fill: parent
                anchors.bottomMargin: units.gu(5)
                footer: Divider {}

                model: billItems

                delegate: BillItemListItem {
                    date: model.date
                    description: model.description
                    price: model.price

                    onRemove: {
                        UITK_Popups.PopupUtils.open(removeConfirmationComponent)
                    }

                    //onClicked: {
                    //    itemsListPage.editItem(model.index)
                    //}

                    Component {
                        id: removeConfirmationComponent
                        ConfirmationPopup {
                            onConfirmed: billItems.remove(model.index, model.biid)
                            text: i18n.tr("Do you really want to remove the item %1 entered on %2?")
                                                .arg(model.description).arg(model.date)
                                                //.arg(model.description).arg(new Date(Date(model.date)).toLocaleDateString(Qt.locale(), i18n.tr("dd.MM.yyyy")))
                            confirmButtonText: i18n.tr("Delete")
                            confirmButtonColor: UITK.UbuntuColors.red
                        }
                    }
                }
            }
        }

        UITK.OptionSelector {
            id: productFavourites

            property var isSelected: []
            property var isReady: []
            visible: clientName != ""

            Layout.fillWidth: true
            Layout.alignment: Qt.AlignBottom
            Layout.leftMargin: units.gu(2)
            Layout.rightMargin: units.gu(2)

            multiSelection: true
            expanded: true
            text: i18n.tr("Quick add:")
            model: products
            delegate: productDelegate

            onDelegateClicked: {
                // hack around buggy multiselection (first click on each item is ignored)
                if (isReady[index]) {
                    isSelected[index] = !isSelected[index];
                }
                else {
                    isReady[index] = true;
                }
            }

            // needed for taming buggy multiselection
            Component.onCompleted: {
                selectedIndex = 9999;
                for (var i=0; i<products.rowCount(); i++) {
                    isSelected[i] = false;
                    isReady[i] = false;
                }
            }
        }

        Component {
            id: productDelegate
            UITK.OptionSelectorDelegate {
                text: title
            }
        }

        RowLayout {
            spacing: units.gu(2)
            Layout.fillWidth: true
            Layout.leftMargin: units.gu(2)
            Layout.rightMargin: units.gu(2)
            Layout.bottomMargin: units.gu(2)

            visible: clientName != ""
           
            FormTextInputField {
                id: date
                fieldName: i18n.tr("carried out at")
                Layout.minimumHeight: units.gu(6)
                Layout.preferredWidth: (parent.width - units.gu(4))/2
                inputMask: i18n.tr("00.00.0000")
                color: isValidDate(date.text) ? "black" : UITK.UbuntuColors.red
                placeHolderText: i18n.tr("dd.mm.yyyy")
            }

            UITK.Button {
                text: "Add to bill items"
                color: UITK.UbuntuColors.green
                onClicked: {
                    for (var i=0; i<products.rowCount(); i++) {
                        var product = products.get(i);
                        if (productFavourites.isSelected[i] && isValidDate(date.text)) {
                            billItems.add(billItems.create(Date.fromLocaleString(Qt.locale(), date.text, i18n.tr("dd.MM.yyyy")),
                                                           product.description,
                                                           product.price,
                                                           product.tax));
                        }
                    }
                }
                Layout.fillWidth: true
                Layout.alignment: Qt.AlignTop
            }
        }
    }

    function isValidDate(date) {
        return Date.fromLocaleDateString(Qt.locale(),
                                         date,
                                         i18n.tr("dd.MM.yyyy")).toLocaleDateString(Qt.locale(), i18n.tr("dd.MM.yyyy")) != "";
    }
}
