import QtQuick 2.4
import QtQuick.Controls 2.2
import Ubuntu.Components 1.3 as UITK

UITK.Page {
    id: aboutPage

    header: UITK.PageHeader {
        id: header
        title: i18n.tr('About')
    }

    Flickable {
        id: aboutFlickable
        //anchors.top: header.bottom
        anchors.fill: parent
        anchors.topMargin: units.gu(2) + header.height

        Column {
            id: aboutCloumn
            spacing: units.gu(2)
            width: parent.width

            UITK.UbuntuShape {
                width: 120
                height: 120
                anchors.horizontalCenter: parent.horizontalCenter
                source: Image {
                    mipmap: true
                    //source: "file:///" + applicationDirPath + "/assets/cash-register-icon.png"
                    source: "file:assets/cash-register-icon.png"
                }
            }

            Item {
                height: nameAndVersionLayout.height
                width: nameAndVersionLayout.width
                anchors.horizontalCenter: parent.horizontalCenter

                UITK.ListItemLayout {
                    id: nameAndVersionLayout
                    padding {
                        top: units.gu(0)
                        bottom: units.gu(2)
                    }

                    title.text: i18n.tr("Ubility")
                    title.font.pixelSize: units.gu(3)
                    title.color: theme.palette.normal.backgroundText
                    title.horizontalAlignment: Text.AlignHCenter

                    subtitle.text: {
                        //i18n.tr("Version %1").arg(Qt.application.version);   
                        i18n.tr("Version %1").arg("1.1.1");   
                    }
                    subtitle.color: UITK.UbuntuColors.ash
                    subtitle.font.pixelSize: units.gu(1.75)
                    subtitle.horizontalAlignment: Text.AlignHCenter
                }
            }
        }

        ListView {
            id: listViewAbout
            anchors {
                top: aboutCloumn.bottom
                bottom: parent.bottom
                left: parent.left
                right: parent.right
                topMargin: units.gu(2)
            }

            currentIndex: -1
            interactive: false

            model: [
                { name: i18n.tr("Get the source"), url: "https://gitlab.com/hummlbach/ubility" },
                { name: i18n.tr("Report issues"),  url: "https://gitlab.com/hummlbach/ubility/issues" },
                { name: i18n.tr("Creator of the app icon"),  url: "http://ebiene.de" },
                { name: i18n.tr("Bill template provided by..."),  url: "https://kiefer-networks.de" },
                { name: i18n.tr("CSS partly taken from bulma"),  url: "https://github.com/jgthms/bulma" }
                //{ name: i18n.tr("Help translate"), url: "https://translate.ubports.com/projects/ubports/telegram-app" }
            ]

            delegate: UITK.ListItem {
                UITK.ListItemLayout {
                    title.text : modelData.name
                    UITK.Icon {
                        width:units.gu(2)
                        name:"go-next"
                    }
                }
                onClicked: Qt.openUrlExternally(modelData.url)
            }
        }
    }
}
