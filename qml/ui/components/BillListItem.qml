import QtQuick 2.7
import Ubuntu.Components 1.3 as UITK
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

UITK.ListItem {
    width: parent.width

    signal cancel()
    signal undo()

    property var billId: null
    property var clientName: null
    property var total: null
    property var dueDate: null

    leadingActions: UITK.ListItemActions {
        actions: [
            UITK.Action {
                iconName: "delete"
                text: i18n.tr("Cancel")
                onTriggered: cancel()
            },
            UITK.Action {
                iconName: "undo"
                text: i18n.tr("Undo")
                onTriggered: undo()
            }
        ]
    }

    ColumnLayout {
        spacing: units.gu(1)
        Layout.fillWidth: true
        anchors {
            left: parent.left
            right: parent.right
            margins: units.gu(2)
        }

        RowLayout {
            spacing: units.gu(2)
            Layout.fillWidth: true
            Layout.alignment: Qt.AlignVCenter

            Label {
                elide: Text.ElideRight
                text: i18n.tr("Bill Id:")
                Layout.alignment: Qt.AlignLeft
                Layout.topMargin: units.gu(1)
            }

            Label {
                elide: Text.ElideRight
                text: billId
                Layout.alignment: Qt.AlignLeft
                Layout.topMargin: units.gu(1)
                Layout.fillWidth: true
            }


            Label {
                elide: Text.ElideRight
                text: i18n.tr("Total: %1").arg(total)
                Layout.alignment: Qt.AlignRight
            }
        }

        RowLayout {
            spacing: units.gu(2)
            Layout.fillWidth: true
            Layout.alignment: Qt.AlignVCenter

            Label {
                elide: Text.ElideRight
                text: i18n.tr("Client:")
                Layout.alignment: Qt.AlignLeft
            }

            Label {
                elide: Text.ElideRight
                text: clientName
                Layout.fillWidth: true
                Layout.alignment: Qt.AlignLeft
            }

            Label {
                elide: Text.ElideRight
                text: i18n.tr("Due: %1").arg(dateLocalization(dueDate))
                Layout.alignment: Qt.AlignRight
            }
        }
    }

    function dateLocalization(dateString) {
        var date = Date.fromLocaleDateString(Qt.locale(), dateString, "yyyy-MM-dd");
        return date.toLocaleDateString(Qt.locale(), i18n.tr("dd.MM.yy"));
    }
}
