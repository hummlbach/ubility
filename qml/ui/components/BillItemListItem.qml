import QtQuick 2.7
import Ubuntu.Components 1.3 as UITK
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

UITK.ListItem {
    width: parent.width

    signal remove()

    property var date: null
    property var description: null
    property var price: null

    leadingActions: UITK.ListItemActions {
        actions: [
            UITK.Action {
                iconName: "delete"
                text: i18n.tr("Remove")
                onTriggered: remove()
            }
        ]
    }

    RowLayout {
        spacing: units.gu(2)
        anchors {
            left: parent.left
            right: parent.right
            margins: units.gu(2)
        }
        Layout.fillWidth: true
        Layout.alignment: Qt.AlignVCenter

        Label {
            elide: Text.ElideRight
            text: dateLocalization(date)
            Layout.alignment: Qt.AlignLeft
            Layout.topMargin: units.gu(1)
        }

        Label {
            elide: Text.ElideRight
            text: description
            Layout.alignment: Qt.AlignHCenter
            Layout.fillWidth: true
            Layout.topMargin: units.gu(1)
        }

        Label {
            elide: Text.ElideRight
            text: price
            Layout.alignment: Qt.AlignRight
            Layout.topMargin: units.gu(1)
        }
    }

    function dateLocalization(dateString) {
        var date = Date.fromLocaleDateString(Qt.locale(), dateString, "yyyy-MM-dd");
        return date.toLocaleDateString(Qt.locale(), i18n.tr("dd.MM.yy"));
    }
}
