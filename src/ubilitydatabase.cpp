#include <QStandardPaths>
#include <QSqlRecord>
#include <QSqlField>
#include <QDir>
#include <QFile>
#include <QUrl>
#include <QDesktopServices>
#include <QPrinter>
#include <QWebView>
#include <QFont>
#include <QDebug>
#include <QDate>

#include "ubilitydatabase.h"

UbilityDatabase::UbilityDatabase(QObject * parent)
    : m_database(QSqlDatabase::addDatabase("QSQLITE"))
{
    QString uri = QStandardPaths::writableLocation(QStandardPaths::DataLocation);
    QString dataDir = uri.replace("file://", "");
    QString databasePath = dataDir + QDir::separator() + "database.db";

    if (!QFile::exists(dataDir)) {
        QDir dir;
        bool createOk = dir.mkpath(dataDir);
        if (!createOk) {
            qWarning() << "Unable to create DB directory" << dataDir;
        }
    }

    m_database.setDatabaseName(databasePath);
    qDebug() << "Opening database " + databasePath;
    if (!m_database.open()) {
        qWarning() << "Failed to open database!";
        return;
    }

    m_clients = new ClientsModel(m_database);
    m_billItems = new BillItemsModel(m_database);
    m_bills = new BillsModel(m_database);
    m_products = new ProductsModel(m_database);
}

Client * UbilityDatabase::currentClient()
{
    QSqlRecord rec = m_clients->record(m_clients->currentClientIndex());
    m_currentClient.firstname = rec.field("firstname").value().toString();
    m_currentClient.lastname = rec.field("lastname").value().toString();
    m_currentClient.street = rec.field("street").value().toString();
    m_currentClient.houseNumber = rec.field("houseNumber").value().toString();
    m_currentClient.postalCode = rec.field("postalCode").value().toString();
    m_currentClient.city = rec.field("city").value().toString();
    m_currentClient.phoneNumber = rec.field("phoneNumber").value().toString();
    m_currentClient.id = rec.field("cid").value().toInt();
    return &m_currentClient;
}

const Bill & UbilityDatabase::currentBill() const
{
    return m_bills->currentBill;
}

ClientsModel * UbilityDatabase::clients() const
{
    return &*m_clients;
}

BillItemsModel * UbilityDatabase::billItems() const
{
    return &*m_billItems;
}

BillsModel * UbilityDatabase::bills() const
{
    return &*m_bills;
}

ProductsModel * UbilityDatabase::products() const
{
    return &*m_products;
}

bool UbilityDatabase::removeClient(int index, int clientId)
{
    m_billItems->removeByClientId(clientId);
    m_clients->remove(index);
    m_bills->removeByClientId(clientId);
    Q_EMIT clientsChanged(); // not sure we really should emit it
    return true;
}

bool UbilityDatabase::setCurrentClient(int clientIndex)
{
    m_clients->setCurrentClientIndex(clientIndex);
    m_billItems->setClientFilter(currentClient()->id);
    m_billItems->setStatusFilter(BillItem::Status::NotYetInvoiced);
    m_billItems->setBillIdFilter(-1);
    Q_EMIT currentClientChanged();
    Q_EMIT billItemsChanged(); // not sure we really should emit it
    return true;
}

bool UbilityDatabase::updateCurrentClient(Client *client)
{
    bool success = m_clients->update(m_clients->currentClientIndex(), *client);
    if (success) {
        Q_EMIT currentClientChanged();
    }
    return success;
}

bool UbilityDatabase::addClient(Client * client)
{
    bool success = m_clients->add(*client);
    if (success) {
        Q_EMIT clientsChanged(); // not sure we really should emit it
    }
    return success;
}

bool UbilityDatabase::setCurrentBill(int index)
{
    m_bills->setCurrentBill(index);
    m_billItems->setBillIdFilter(m_bills->currentBill.id);
    m_billItems->setStatusFilter(BillItem::Status::Undefined);
    m_billItems->setClientFilter(-1);
    Q_EMIT billItemsChanged(); // not sure we really should emit it
}

QString UbilityDatabase::addBill(Biller * biller)
{
    double tax = m_billItems->tax();
    double total = m_billItems->sum() + tax;
    m_bills->add(currentClient()->id,
                 currentClient()->firstname + " " + currentClient()->lastname,
                 total,
                 tax);
    m_billItems->setBillId(m_bills->currentBill.id);
    m_billItems->setStatus(BillItem::Status::Invoiced);
    m_billItems->setStatusFilter(BillItem::Status::Invoiced);
    m_billItems->setBillIdFilter(m_bills->currentBill.id);
    m_billItems->sort(0, Qt::AscendingOrder);

    QString filename = currentBillToPdf(biller);

    m_billItems->setStatusFilter(BillItem::Status::NotYetInvoiced);
    m_billItems->setBillIdFilter(-1);

    Q_EMIT billItemsChanged();
    return filename;
}

QString UbilityDatabase::currentBillToPdf(Biller * biller)
{
    QString docfile = QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation)
	             + "/bill_" + QString::number(m_bills->currentBill.id) + "_" + currentClient()->lastname + ".pdf";
    QString cachefile = QStandardPaths::writableLocation(QStandardPaths::CacheLocation)
	             + "/bill_" + QString::number(m_bills->currentBill.id) + "_" + currentClient()->lastname + ".pdf";
    QString html = currentBillHtml(biller);

    QPrinter printer(QPrinter::PrinterResolution);
    printer.setOutputFileName(cachefile);
    printer.setPaperSize(QPrinter::A4);
    QWebView webview;
    webview.setFont(QFont("Helvetica"));
    webview.setTextSizeMultiplier(3.0);
    webview.setHtml(html);
    webview.print(&printer); 
    printer.setOutputFileName(docfile);
    webview.print(&printer); 
    return cachefile;
}

bool UbilityDatabase::cancelBill(int index, int billId)
{
    m_billItems->removeByBillId(billId);
    m_bills->remove(index);
}

bool UbilityDatabase::undoBill(int index, int billId)
{
    m_billItems->setStatusByBillId(billId, BillItem::Status::NotYetInvoiced);
    m_bills->remove(index);
}

QString UbilityDatabase::currentBillHtml(Biller * biller)
{
    QFile billTemplateFile("assets/bill.html");
    if (!billTemplateFile.open(QIODevice::ReadOnly | QIODevice::Text))
        return "";
    QTextStream billTemplateStream(&billTemplateFile);
    QString billTemplateHtml = billTemplateStream.readAll();
    
    // biller data
    billTemplateHtml.replace("{{from.name}}", biller->firstname+" "+biller->lastname);
    billTemplateHtml.replace("{{from.title}}", biller->title);
    billTemplateHtml.replace("{{from.company}}", biller->company);
    billTemplateHtml.replace("{{from.street}}", biller->street+" "+biller->houseNumber);
    billTemplateHtml.replace("{{from.postcode}}", biller->postalCode);
    billTemplateHtml.replace("{{from.city}}", biller->city);
    billTemplateHtml.replace("{{from.taxId}}", biller->taxId);
    billTemplateHtml.replace("{{from.website}}", "");
    billTemplateHtml.replace("{{from.email}}", "");
    billTemplateHtml.replace("{{from.phoneNumber}}", biller->phoneNumber);
    billTemplateHtml.replace("{{from.faxNumber}}", biller->faxNumber);
    billTemplateHtml.replace("{{from.iban}}", biller->iban);
    billTemplateHtml.replace("{{from.bic}}", biller->bic);
    billTemplateHtml.replace("{{from.bank}}", biller->bank);

    billTemplateHtml.replace("{{currency}}", biller->currency);

   
    // client data
    billTemplateHtml.replace("{{to.name}}", currentClient()->firstname+" "+currentClient()->lastname);
    billTemplateHtml.replace("{{to.street}}", currentClient()->street+" "+currentClient()->houseNumber);
    billTemplateHtml.replace("{{to.postcode}}", currentClient()->postalCode);
    billTemplateHtml.replace("{{to.city}}", currentClient()->city);
    billTemplateHtml.replace("{{customer_number}}", QString::number(currentClient()->id));


    // bill data
    // TODO: how to implement preview? -> method returning bill object instead of using currentBill?
    billTemplateHtml.replace("{{invoice.number}}", QString::number(m_bills->currentBill.id));
    billTemplateHtml.replace("{{invoice.date}}", m_bills->currentBill.dateInvoiced.toString(QObject::tr("dd.MM.yy")));
    billTemplateHtml.replace("{{invoice.pay_until_date}}", m_bills->currentBill.dateRespite.toString(QObject::tr("dd.MM.yy")));
    billTemplateHtml.replace("{{totals.tax}}", QString::number(m_bills->currentBill.taxIncluded));
    billTemplateHtml.replace("{{totals.brutto}}", QString::number(m_bills->currentBill.billingAmount));
    double netto = m_bills->currentBill.billingAmount - m_bills->currentBill.taxIncluded;
    billTemplateHtml.replace("{{totals.netto}}", QString::number(netto));

    QRegExp billItemTemplateMatcher("\\{\\{#positions\\}\\}(.*)\\{\\{/positions\\}\\}");
    int index = billItemTemplateMatcher.indexIn(billTemplateHtml, 0);
    QString billItemTemplate = billItemTemplateMatcher.cap(1);
    billTemplateHtml.replace(billItemTemplateMatcher, "{{position}}");

    m_billItems->sort(1, Qt::AscendingOrder);

    for (int row=0; row<m_billItems->rowCount(); row++) {
        QString billItem = billItemTemplate+"\n{{position}}";
        int dateRole = m_billItems->roleNames().key("date");
        QDate date = m_billItems->data(m_billItems->index(row, 0), dateRole).toDate();
        billItem.replace("{{date}}", date.toString(QObject::tr("dd.MM.yy")));
        int descriptionRole = m_billItems->roleNames().key("description");
        QVariant description = m_billItems->data(m_billItems->index(row, 0), descriptionRole);
        billItem.replace("{{{text}}}", description.toString());
        billItem.replace("{{amount}}", "1");
        //billItem.replace("{{amount_desc}}", "1");
        int priceRole = m_billItems->roleNames().key("price");
        QVariant price = m_billItems->data(m_billItems->index(row, 0), priceRole);
        billItem.replace("{{netto_price}}", QString::number(price.toDouble()));
        billItem.replace("{{total_netto_price}}", QString::number(price.toDouble()));
        int taxRole = m_billItems->roleNames().key("tax");
        QVariant tax = m_billItems->data(m_billItems->index(row, 0), taxRole);
        billItem.replace("{{tax_percent}}", QString::number(tax.toDouble()));
        billTemplateHtml.replace("{{position}}", billItem);
    }
    billTemplateHtml.replace("{{position}}","");

    return billTemplateHtml;
}
