#include "billsmodel.h"

#include <QDebug>
#include <QSqlRecord>
#include <QSqlError>
#include <QSqlQuery>
#include <QSqlField>
#include <QDate>

BillsModel::BillsModel(QSqlDatabase database, QObject *parent)
    : QSqlTableModel(parent, database)
{
    QSqlQuery query(database);
    if (!query.exec("create table if not exists bills "
                    "(bid integer primary key, "
                    "dateInvoiced DATE, "
                    "dateRespite DATE, "
                    "datePayed DATE, "
                    "billingamount float, "
                    "taxincluded float, "
                    "paymentreceived float, "
                    "clientname varchar(30), "
                    "clientid integer)")) {
        qWarning() << "bills table creation: " << lastError().text();
        return;
    }

    setTable("bills");
    if (!select()) {
        qWarning() << "Failed to select table bills: " << lastError().text();
    }
    sort(0, Qt::DescendingOrder);
}

QVariant BillsModel::data(const QModelIndex &index, int role) const
{
    QVariant value;

    if (index.isValid()) {
        if (role < Qt::UserRole) {
            value = QSqlTableModel::data(index, role);
        } else {
            int columnIdx = role - Qt::UserRole - 1;
            QModelIndex modelIndex = this->index(index.row(), columnIdx);
            value = QSqlTableModel::data(modelIndex, Qt::DisplayRole);
        }
    }
    return value;
}

QHash<int, QByteArray> BillsModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    for (int i = 0; i < this->record().count(); i++) {
        roles.insert(Qt::UserRole + i + 1, record().fieldName(i).toUtf8());
    }
    return roles;
}

QSqlRecord BillsModel::createRecord(int clientId, const QString &clientName, double billingAmount, double taxIncluded) const
{
    QSqlRecord newRecord;
    newRecord.append(QSqlField("dateinvoiced", QVariant::Date));
    newRecord.append(QSqlField("daterespite", QVariant::Date));
    newRecord.append(QSqlField("billingamount", QVariant::Double));
    newRecord.append(QSqlField("taxincluded", QVariant::Double));
    newRecord.append(QSqlField("clientid", QVariant::Int));
    newRecord.append(QSqlField("clientname", QVariant::String));
    newRecord.setValue("dateinvoiced", QDate::currentDate());
    newRecord.setValue("daterespite", QDate::currentDate().addDays(14));
    newRecord.setValue("billingamount", billingAmount);
    newRecord.setValue("taxincluded", taxIncluded);
    newRecord.setValue("clientid", clientId);
    newRecord.setValue("clientname", clientName);
    return newRecord;
}

bool BillsModel::setCurrentBill(const QSqlRecord &record)
{
    currentBill = Bill(record.field("dateinvoiced").value().toDate(),
                       record.field("daterespite").value().toDate(),
                       QDate(),
                       record.field("billingamount").value().toDouble(),
                       record.field("taxincluded").value().toDouble(),
                       0,
                       record.field("bid").value().toInt(),
                       record.field("clientid").value().toInt(),
                       record.field("clientname").value().toString());
    return true;
}

bool BillsModel::setCurrentBill(int index)
{
    QSqlRecord rec = record(index);
    return setCurrentBill(rec);
}

bool BillsModel::add(int clientId, const QString &clientName, double billingAmount, double taxIncluded)
{
    QSqlRecord newRecord = createRecord(clientId, clientName, billingAmount, taxIncluded);
    if (!insertRecord(-1, newRecord)) {
        qWarning() << "Failed to insert record into item database: " << lastError().text();
        return false;
    }
    select();
    newRecord = record(rowCount()-1);
    return setCurrentBill(newRecord);
}

BillsModel::~BillsModel() {
    database().close();
}

bool BillsModel::removeByClientId(int clientId)
{
    QSqlQuery query(database());
    beginResetModel();
    if (!query.exec("delete from bills where clientid='" + QString::number(clientId) + "'")) {
        qWarning() << "Failed to remove bills for client with id " << clientId << ": " << lastError().text();
        return false;
    }
    endResetModel();
}

bool BillsModel::remove(int index)
{
    beginRemoveRows(QModelIndex(), index, index);
    removeRow(index);
    endRemoveRows();
    return true;
}

