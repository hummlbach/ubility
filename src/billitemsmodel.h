#ifndef BILLITEMSMODEL_H
#define BILLITEMSMODEL_H

#include <QSqlDatabase>
#include <QSqlTableModel>
#include <QSqlRecord>
#include <QDate>

class BillItem
{
    Q_GADGET
    Q_PROPERTY(QDate date MEMBER date)
    Q_PROPERTY(QString description MEMBER description)
    Q_PROPERTY(double price MEMBER price)
    Q_PROPERTY(double tax MEMBER tax)
    Q_PROPERTY(Status status MEMBER status)
    Q_PROPERTY(int billId MEMBER billId)

public:
    enum Status {
        Undefined = -1,
        NotYetInvoiced = 1,
        Invoiced = 2,
        Payed = 3,
        Cancelled = 4,
    };
    Q_ENUM(Status)

    explicit BillItem() {};
    explicit BillItem(const QDate &date, const QString &description, double price, double tax, Status status, int billId)
        : date(date), description(description), price(price), tax(tax), status(status), billId(billId)
    {};
    BillItem(const BillItem &item)
        : date(item.date), description(item.description), price(item.price), tax(item.tax), status(item.status), billId(item.billId)
    {};

    QDate date;
    QString description;
    double price;
    double tax;
    Status status;
    int billId;
};

typedef BillItem::Status BillItemStatus;

class BillItemsModel : public QSqlTableModel
{
    Q_OBJECT

public:
    explicit BillItemsModel(QSqlDatabase database, QObject *parent = 0);
    explicit BillItemsModel(const BillItemsModel & billItemsModel) {}
    explicit BillItemsModel() {}
    virtual ~BillItemsModel();

    QVariant data(const QModelIndex &index, int role) const;
    QHash<int, QByteArray> roleNames() const;

    Q_INVOKABLE bool add(const BillItem &item);
    Q_INVOKABLE bool remove(int index, int billItemId);
    Q_INVOKABLE BillItem create(const QDate &date, const QString &description, double price, double tax=0.0) const;

    bool setBillId(int billId);
    bool setStatus(BillItem::Status status);
    bool setStatusByBillId(int billId, BillItem::Status status);
    bool removeByClientId(int clientId);
    bool removeByBillId(int billId);
    double sum() const;
    double tax() const;
    //Q_INVOKABLE QVariantList<BillItem> getCurrent() const;

    int clientId() const;
    void setClientFilter(int clientId);
    BillItem::Status status() const;
    void setStatusFilter(BillItem::Status status);
    int billId() const;
    void setBillIdFilter(int billId);

Q_SIGNALS:
    void databaseChanged();
    void filterChanged();

private:
    QSqlRecord createRecord(const BillItem &item) const;
    void updateFilter();
    int m_clientId;
    BillItem::Status m_status;
    int m_billId;
};

#endif
